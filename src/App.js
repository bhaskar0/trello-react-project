import React, { Component } from "react";
import Home from "./components/home";
import Routes from "./components/router";

class App extends Component {
  state = {};
  render() {
    return <Routes />;
  }
}

export default App;
