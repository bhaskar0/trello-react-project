import React, { Component } from "react";

class CardComponent extends Component {
  state = {};

  render() {
    return (
      <div className="card m-2 listItems">
        <div className="card-body p-0 d-flex align-items-center justify-content-between Card">
          <div
            className="cardContent w-100 p-4"
            onClick={() => this.props.showModal(this.props.cardId)}
          >
            {this.props.name}
          </div>
          <button
            type="button"
            className="btn btn-dangers deleteBtn"
            onClick={() => this.props.onDelete(this.props.cardId)}
          >
            &#10799;
          </button>
        </div>
      </div>
    );
  }
}

export default CardComponent;
