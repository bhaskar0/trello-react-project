import React, { Component } from "react";
import Form from "./form";
import CardComponent from "./cardComponent";
import ModalComponent from "./modalComponent";

class Card extends Component {
  state = {
    card: [],
    formShow: false,
    // apikey: "a8b3a46e482a943fa2cf6b653054491e",
    // token: "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8",
    // boardId: "5e855f3d0a0e4e0g5j7i0a78cd6d2d",
    // listId: this.props.id,
    cardOpen: "",
  };
  componentDidMount = async () => {
    const response = await fetch(
      `https://api.trello.com/1/lists/${this.props.id}/cards?key=${this.props.apikey}&token=${this.props.token}`
    );
    const data = await response.json();
    data.forEach((card) => this.handleAddCard(card.name, card.id));
  };

  handleAddCard = (cardName, cardId) => {
    this.setState((currentState) => {
      return {
        card: currentState.card.concat([
          { name: cardName, cardId: cardId, showCardModal: false },
        ]),
      };
    });
  };

  handleAddCardForm = (event) => {
    this.setState({ formShow: true });
  };

  handleSubmit = (value) => {
    event.preventDefault();
    if (value) {
      this.setState({ formShow: false });
      this.createCard(value);
    } else {
      alert("please enter the value");
    }
  };

  createCard = async (cardName) => {
    // console.log("cad Created");
    const card = await fetch(
      `https://api.trello.com/1/cards?idList=${this.props.id}&name=${cardName}&key=${this.props.apikey}&token=${this.props.token}`,
      { method: "POST" }
    );
    const cardData = await card.json();
    this.handleAddCard(cardData.name, cardData.id);
  };

  handleDeleteCard = async (deleteCardId) => {
    const cardDelete = await fetch(
      `https://api.trello.com/1/cards/${deleteCardId}?key=${this.props.apikey}&token=${this.props.token}`,
      { method: "DELETE" }
    );
    console.log(cardDelete.status);
    this.setState({
      card: this.state.card.filter((card) => card.cardId != deleteCardId),
    });
  };
  onDeleteForm = () => {
    this.setState({ formShow: false });
  };

  handleModalRemove = (cardId) => {
    this.setState({ cardOpen: "" });
  };

  handleCardOpen = (id) => {
    const cardSelected = this.state.card.filter((card) => card.cardId === id);
    cardSelected[0].showCardModal = true;
    this.setState({ cardOpen: cardSelected[0] });
  };

  render() {
    // console.log(this.props);
    const { card } = this.state;
    return (
      <div>
        <div className="m-1" style={{ backgroundColor: "#eee" }}>
          <h4
            style={{
              textAlign: "center",
              marginTop: "2vh",
              padding: "0.5em",
            }}
          >
            {this.props.name}
          </h4>
          {card.map((card) => (
            <CardComponent
              name={card.name}
              key={card.cardId}
              cardId={card.cardId}
              showModal={this.handleCardOpen}
              onDelete={this.handleDeleteCard}
            />
          ))}
          {this.state.cardOpen ? (
            <ModalComponent
              key={this.state.cardOpen.cardId}
              onModalremove={this.handleModalRemove}
              cardId={this.state.cardOpen.cardId}
              cardName={this.state.cardOpen.name}
              apikey={this.props.apikey}
              token={this.props.token}
            />
          ) : null}

          {this.state.formShow ? (
            <Form submit={this.handleSubmit} onDelete={this.onDeleteForm} />
          ) : (
            <button
              type="button"
              className="btn btn-outline-secondary"
              onClick={this.handleAddCardForm}
              style={{ minWidth: "40vh", border: "none" }}
            >
              add a Card
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default Card;
