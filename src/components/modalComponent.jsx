import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Checklists from "./checklists";
import Form from "./form";

class ModalContent extends Component {
  state = {
    show: true,
    checklists: [],
    formShow: false,
  };

  componentDidMount = async () => {
    // console.log(this.props.cardId);
    const CardComponents = await fetch(
      `https://api.trello.com/1/cards/${this.props.cardId}/checklists?key=${this.props.apikey}&token=${this.props.token}`,
      {
        method: "GET",
      }
    );
    const cardData = await CardComponents.json();
    // console.log(cardData);
    cardData.forEach((checklist) =>
      this.handleAddChecklists(
        checklist.id,
        checklist.name,
        checklist.checkItems
      )
    );
  };

  handleAddChecklists = (checklistId, checklistName, checkItems) => {
    this.setState((currentState) => {
      return {
        checklists: currentState.checklists.concat([
          {
            checklistName: checklistName,
            checklistId: checklistId,
            checkItems: checkItems,
          },
        ]),
      };
    });
  };

  handleClose = () => {
    this.setState({ show: false });
    this.props.onModalremove(this.props.cardId);
  };

  createChecklistForm = () => {
    this.setState({ formShow: true });
  };

  handleSubmit = (value) => {
    event.preventDefault();
    if (value) {
      this.setState({ formShow: false });
      this.createChecklist(value);
    } else {
      alert("please enter the value");
    }
  };

  createChecklist = async (value) => {
    const checklist = await fetch(
      `https://api.trello.com/1/checklists?idCard=${this.props.cardId}&name=${value}
      &key=${this.props.apikey}&token=${this.props.token}`,
      {
        method: "POST",
      }
    );
    const data = await checklist.json();
    this.handleAddChecklists(data.id, data.name, data.checkItems);
  };

  handelDeleteChecklist = async (deleteChecklistId) => {
    // console.log(id);
    const deleteChecklist = await fetch(
      `https://api.trello.com/1/checklists/${deleteChecklistId}?key=${this.props.apikey}&token=${this.props.token}`,
      {
        method: "DELETE",
      }
    );
    console.log(this.state.checklists);

    this.setState({
      checklists: this.state.checklists.filter(
        (checklist) => checklist.checklistId != deleteChecklistId
      ),
    });
  };

  onDeleteForm = () => {
    this.setState({ formShow: false });
  };

  render() {
    console.log(this.state.checklists);
    const { show, checklists } = this.state;
    return (
      <div>
        <Modal show={show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>{this.props.cardName}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.formShow ? (
              <Form submit={this.handleSubmit} onDelete={this.onDeleteForm} />
            ) : (
              <button
                type="button"
                className="btn btn-primary"
                onClick={this.createChecklistForm}
              >
                Add CheckList
              </button>
            )}
            {checklists.map((checklist) => (
              <Checklists
                name={checklist.checklistName}
                key={checklist.checklistId}
                id={checklist.checklistId}
                checkItems={checklist.checkItems}
                onDelete={this.handelDeleteChecklist}
                cardId={this.props.cardId}
                apikey={this.props.apikey}
                token={this.props.token}
              />
            ))}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
              close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalContent;
