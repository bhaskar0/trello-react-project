import React, { Component } from "react";
import Card from "./Card";
import Form from "./form";

const apikey = "a8b3a46e482a943fa2cf6b653054491e";
const token =
  "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8";

class List extends Component {
  state = {
    lists: [],
    boardId: this.props.match.params.boardId,
    formShow: false,
  };

  componentDidMount = async () => {
    const getLists = await fetch(
      `https://api.trello.com/1/boards/${this.state.boardId}/lists?key=${apikey}&token=${token}`
    );
    const lists = await getLists.json();
    this.setState({ lists });
  };

  createList = async (listName) => {
    const createList = await fetch(
      `https://api.trello.com/1/lists?name=${listName}&idBoard=${this.state.boardId}&key=${apikey}&token=${token}`,
      {
        method: "POST",
      }
    );
    const createdList = await createList.json();
    this.setState({ lists: this.state.lists.concat(createdList) });
  };

  handleAddCardForm = () => {
    this.setState({ formShow: true });
  };

  handleSubmit = (value) => {
    event.preventDefault();
    if (value) {
      this.setState({ formShow: false });
      this.createList(value);
    } else {
      alert("please enter the value");
    }
  };
  onDeleteForm = () => {
    this.setState({ formShow: false });
  };

  render() {
    // console.log(this.props);
    const { lists } = this.state;
    return (
      <div>
        <h3 style={{ textAlign: "center", fontWeight: "bold" }}>
          {this.props.match.params.boardName}
        </h3>
        <div className="d-flex lists">
          {lists.map((list) => (
            <Card
              key={list.id}
              id={list.id}
              name={list.name}
              apikey={apikey}
              token={token}
            />
          ))}
          {this.state.formShow ? (
            <Form submit={this.handleSubmit} onDelete={this.onDeleteForm} />
          ) : (
            <button
              type="button"
              className="btn btn-secondary"
              onClick={this.handleAddCardForm}
              style={{
                minWidth: "40vh",
                height: "4vh",
                border: "none",
                margin: "1em",
              }}
            >
              + add a list
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default List;
