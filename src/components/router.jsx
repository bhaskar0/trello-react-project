import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./home";
import List from "./list";

class Routes extends Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/boards/:boardName/:boardId" component={List} />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
