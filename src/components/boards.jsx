import React, { Component } from "react";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter, Switch, Link, Route } from "react-router-dom";

class Boards extends Component {
  state = {
    formShow: false,
  };

  render() {
    const { lists } = this.state;
    return (
      <Link to={`/boards/${this.props.boardName}/${this.props.boardId}`}>
        <div
          className="card m-4 boards shadow p-3 mb-5 bg-white rounded"
          style={{
            width: "18rem",
            cursor: "pointer",
          }}
        >
          <div className="card-body">
            <h4 className="card-title mb-4">{this.props.boardName}</h4>
          </div>
        </div>
      </Link>
    );
  }
}

export default Boards;
