import React from "react";
import CheckItems from "./checkItems";
import Form from "./form";

class Checklist extends React.Component {
  state = {
    checkItems: [],
    formShow: false,
    // apikey: "a8b3a46e482a943fa2cf6b653054491e",
    // token: "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8",
  };

  componentDidMount = () => {
    this.setState({
      checkItems: this.state.checkItems.concat(this.props.checkItems),
    });
  };
  deleteCheckItem = async (checklistId, checkitemId) => {
    const deleteCheckItem = await fetch(
      `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkitemId}?key=${this.props.apikey}&token=${this.props.token}`,
      {
        method: "DELETE",
      }
    );

    this.setState({
      checkItems: this.state.checkItems.filter(
        (checkItem) => checkItem.id !== checkitemId
      ),
    });
  };

  handleAddCheckitem = async (checkitemName) => {
    const createCheckItem = await fetch(
      `https://api.trello.com/1/checklists/${this.props.id}/checkItems?name=${checkitemName}
      &key=${this.props.apikey}&token=${this.props.token}`,
      {
        method: "POST",
      }
    );
    const checkItem = await createCheckItem.json();
    // console.log(checkItem);
    this.setState({ checkItems: this.state.checkItems.concat(checkItem) });
  };

  createCheckitemsForm = () => {
    this.setState({ formShow: true });
  };

  handleSubmit = (value) => {
    event.preventDefault();
    if (value) {
      this.setState({ formShow: false });
      this.handleAddCheckitem(value);
    } else {
      alert("please enter the value");
    }
  };

  onDeleteForm = () => {
    this.setState({ formShow: false });
  };

  handleChange = (id, checkitemState) => {
    if (checkitemState === "incomplete") {
      checkitemState = "complete";
    } else {
      checkitemState = "incomplete";
    }

    fetch(
      `https://api.trello.com/1/cards/${this.props.cardId}/checkItem/${id}?key=${this.props.apikey}&token=${this.props.token}&state=${checkitemState}`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.text();
      })
      .then((text) => console.log(text));
  };

  render() {
    const { checkItems } = this.state;
    return (
      <div className="card m-2 position-relative">
        <div className="card-body">
          <div className="cardContent w-100 p-2">
            <h4>{this.props.name}</h4>
            {checkItems.map((checkItem) => (
              <CheckItems
                status={checkItem.state === "complete" ? true : false}
                key={checkItem.id}
                checkItem={checkItem}
                onDelete={this.deleteCheckItem}
                handleChange={this.handleChange}
              />
            ))}
          </div>
          <button
            type="button"
            className="btn btn-dangers ChecklistDeleteBtn"
            onClick={() => this.props.onDelete(this.props.id)}
          >
            &#10799;
          </button>
          {this.state.formShow ? (
            <Form submit={this.handleSubmit} onDelete={this.onDeleteForm} />
          ) : (
            <button
              type="button"
              className="btn btn-success"
              onClick={this.createCheckitemsForm}
            >
              Add item
            </button>
          )}
        </div>
      </div>
    );
  }
}

export default Checklist;
