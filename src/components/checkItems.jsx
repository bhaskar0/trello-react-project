import React, { Component } from "react";

class CheckItems extends Component {
  state = {
    checkItem: this.props.checkItem,
    isChecked: this.props.status,
  };

  handleChange = (event) => {
    this.setState(
      {
        isChecked: event.target.checked,
      },
      () =>
        this.props.handleChange(
          this.props.checkItem.id,
          this.props.checkItem.state
        )
    );
  };

  render() {
    console.log(this.state);
    return (
      <div className="d-flex align-items-center justify-content-between">
        <div>
          <input
            type="checkbox"
            onChange={(event) => this.handleChange(event)}
            checked={this.state.isChecked}
          />
          <span className="m-4">{this.props.checkItem.name}</span>
        </div>
        <button
          type="button"
          className="btn btn-dangers float-right "
          onClick={() =>
            this.props.onDelete(
              this.props.checkItem.idChecklist,
              this.props.checkItem.id
            )
          }
        >
          &#10799;
        </button>
      </div>
    );
  }
}

export default CheckItems;
