import React, { Component } from "react";
import List from "./Card";
import Boards from "./boards";
import Form from "./form";

const apiKey = "a8b3a46e482a943fa2cf6b653054491e";
const token =
  "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8";

class Home extends Component {
  state = {
    boards: [],
    formShow: false,
    // apikey: "a8b3a46e482a943fa2cf6b653054491e",
    // token: "ca2363ed3179ae37afcb7294a54212293e824de0430efe0c2e1a8ac395c4c7a8",
  };

  componentDidMount = async () => {
    const boardsData = await fetch(
      `https://api.trello.com/1/members/me/boards?key=${apiKey}&token=${token}`,
      {
        method: "GET",
      }
    );
    const boards = await boardsData.json();
    const avilableBoards = boards.filter((board) => !board.closed);

    avilableBoards.forEach((board) => this.handleAddBoards(board));
  };
  createBoard = async (boardName) => {
    const createBoard = await fetch(
      `https://api.trello.com/1/boards?name=${boardName}&key=${apiKey}&token=${token}`,
      {
        method: "POST",
      }
    );
    const createdBoard = await createBoard.json();
    this.handleAddBoards(createdBoard);
  };

  handleAddBoards = (board) => {
    this.setState({ boards: this.state.boards.concat(board) });
  };

  createAddCardForm = () => {
    this.setState({ formShow: true });
  };

  onDeleteForm = () => {
    this.setState({ formShow: false });
  };

  handleSubmit = (value) => {
    event.preventDefault();
    if (value) {
      this.setState({ formShow: false });
      this.createBoard(value);
    } else {
      alert("please enter the value");
    }
  };
  handleDeleteBoards = async (deleteBoardId) => {
    const deleteCard = await fetch(
      `https://api.trello.com/1/boards/${deleteBoardId}?key=${apiKey}&token=${token}`,
      { method: "DELETE" }
    );
    this.setState({
      boards: this.state.boards.filter((board) => board.id !== deleteBoardId),
    });
  };

  render() {
    const { boards } = this.state;
    // console.log(apiKey);
    // console.log(this.state.boards);
    return (
      <div>
        <h1 style={{ textAlign: "center", margin: "1rem" }}>Trello Boards</h1>

        <div className="d-flex flex-wrap justify-content-center align-items-center">
          {this.state.formShow ? (
            <Form submit={this.handleSubmit} onDelete={this.onDeleteForm} />
          ) : (
            <div
              className="card m-4 boards addBoard shadow p-3 mb-5 rounded"
              onClick={this.createAddCardForm}
              style={{
                width: "18rem",
                cursor: "pointer",
              }}
            >
              <div className="card-body">
                <h4 className="card-title mb-4">+ Create new board</h4>
              </div>
            </div>
          )}
          {boards.map((board) => (
            <Boards
              key={board.id}
              boardId={board.id}
              boardName={board.name}
              apiKey={apiKey}
              token={token}
              // onDelete={this.handleDeleteBoards}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Home;
