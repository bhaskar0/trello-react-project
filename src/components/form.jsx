import React, { Component } from "react";

class Form extends Component {
  state = {
    value: "",
  };
  onChange = (event) => {
    this.setState({ value: event.target.value });
  };

  render() {
    console.log(this.state.value);
    return (
      <div>
        <button
          type="button"
          className="btn btn-dangers deleteBtn float-right"
          onClick={() => this.props.onDelete(this.props.cardId)}
        >
          &#10799;
        </button>
        <form onSubmit={() => this.props.submit(this.state.value)}>
          <input
            className="m-2 "
            type="text"
            value={this.state.value}
            onChange={this.onChange}
            placeholder="enter the Value"
          />
          <br />
          <button type="submit" className="m-2">
            Submit
          </button>
        </form>
      </div>
    );
  }
}

export default Form;
